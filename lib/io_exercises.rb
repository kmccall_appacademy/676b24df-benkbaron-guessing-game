# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  secret_num = rand(99) + 1
  puts "Guess a number."
  guess = gets.chomp.to_i
  puts guess
  guess_number = 1
  until guess == secret_num
    if guess > secret_num
      puts guess
      puts "too high"
    else guess < secret_num
      puts guess
      puts "too low"
    end
    guess = gets.chomp.to_i
    guess_number += 1
  end
  puts secret_num
  puts guess_number
end

def file_shuffler
  puts "What's the file's name?"
  file = gets.chomp

  line_arr = File.readlines(file).shuffle
  new_file = File.new("#{file}-shuffled.txt")

  File.open("#{file}-shuffled.txt", "w") do
  line_arr.each { |line| puts line }
  end
end

file_shuffler
#  Write file_shuffler program that: * prompts the user for a file name *
#reads that file * shuffles the lines * saves it to the file "{input_name}-shuffled.txt".

#Hint: You could create a random number using the Random class, or you could use the shuffle method in Array.
